/**
 * Created by drif hosseine on 12/11/2016.
 */
var mongoose = require('mongoose');
var conf = require('./confenv');
var customError = require('./backend/errors/customError');
var objectError = require('./backend/errors/objectError');
mongoose.connect(conf.mongoUrl, function (err) {
    if (err) {
        throw new customError(objectError.serverDbError);
    }
});
module.exports = mongoose;