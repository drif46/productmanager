/**
 * Created by hdf on 05/12/2016.
 */
angular.module('app').service('UserSvc', function($http){
    this.saveUser = function(user){
        this.user = user;
    };

    this.getUser = function(){
        return this.user;
    };

    this.adminAuthentication = function (email, password) {
        return $http.post('/admin/authenticate/', {
            email: email,
            password: password
        });
    };
});