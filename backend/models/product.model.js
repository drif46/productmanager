/**
 * Created by drif hosseine on 12/11/2016.
 */
var mongoose = require('../../db');

var productSchema = {
    category: {type: String},
    buyPrice: {type: Number},
    sellPrice: {type: Number},
    brand: {type: String},
    weight: {type: Number},
    creationDate: {type: Date},
    updateDate: {type: Date},
    amount: {type: Number},
    productDetails: {type: mongoose.Schema.Types.Mixed}
};

module.exports = mongoose.model('Product', productSchema);