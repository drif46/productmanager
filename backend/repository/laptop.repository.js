/**
 * Created by drif hosseine on 14/11/2016.
 */
var laptopType = require('../entities/laptop');
var customError = require('../errors/customError');
var objectError = require('../errors/objectError');

function LaptopRepository() {
    var productRepository = require('../repository/product.repository');
    this.productRepository = new productRepository();
}

LaptopRepository.prototype.createLaptop = function (laptop, cb) {
    if (laptop instanceof laptopType) {
        this.productRepository.createProduct(laptop, cb);
    } else {
        cb(new customError(objectError.laptopTypeError));
    }
};

LaptopRepository.prototype.listAllLaptop = function (cb) {
    this.productRepository.findByCategory('Laptop', cb);
};

module.exports = LaptopRepository;

