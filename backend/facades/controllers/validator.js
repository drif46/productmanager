/**
 * Created by drif hosseine on 22/11/2016.
 */
var Validator = (function () {
    var _validator = require('validator');
    var _laptop = require('../../entities/laptop');
    var _admin = require('../../entities/admin');
    var _customError = require('../../errors/customError');
    var _objectError = require('../../errors/objectError');
    function checkCategory(category) {
        var isValid = false;
        if (category) {
            category = category.toString();
            if (_validator.isIn(category, ['Laptop']))
                isValid = true;
        }
        return isValid;
    }

    function checkBuyPrice(buyPrice) {
        var isValid = false;
        if (buyPrice) {
            buyPrice = buyPrice.toString();
            if (_validator.isFloat(buyPrice, {min: 0.0}))
                isValid = true;
        }
        return isValid;
    }

    function checkSellPrice(sellPrice) {
        var isValid = false;
        if (sellPrice) {
            sellPrice = sellPrice.toString();
            if (_validator.isFloat(sellPrice, {min: 0.0}))
                isValid = true;
        }
        return isValid;
    }

    function checkBrand(brand) {
        var isValid = false;
        if (!brand) {
            isValid = true;
        }

        if (brand) {
            brand = brand.toString();
            if (!_validator.isInt(brand) && !_validator.isFloat(brand) && !_validator.isNumeric(brand))
                isValid = true;
        }
        return isValid;
    }

    function checkWeight(weight) {
        var isValid = false;
        if (!weight) {
            isValid = true;
        }

        if (weight) {
            weight = weight.toString();
            if (_validator.isFloat(weight, {min: 0.0}))
                isValid = true;
        }
        return isValid;
    }

    function checkCreationDate(creationDate) {
        var isValid = false;
        if (!creationDate) {
            isValid = true;
        }

        if (creationDate) {
            creationDate = creationDate.toString();
            if (_validator.isDate(creationDate))
                isValid = true;
        }
        return isValid;
    }

    function checkUpdateDate(updateDate) {
        var isValid = false;
        if (!updateDate) {
            isValid = true;
        }

        if (updateDate) {
            updateDate = updateDate.toString();
            if (_validator.isDate(updateDate))
                isValid = true;
        }
        return isValid;
    }

    function checkAmount(amount) {
        var isValid = false;
        if (amount) {
            amount = amount.toString();
            if (_validator.isInt(amount, {min: 0}))
                isValid = true;
        }
        return isValid;
    }

    function checkProduct(product) {
        var isValid = product && checkCategory(product.category) && checkBuyPrice(product.buyPrice)
            && checkSellPrice(product.sellPrice) && checkBrand(product.brand) && checkWeight(product.weight)
            && checkCreationDate(product.creationDate) && checkUpdateDate(product.updateDate) && checkAmount(product.amount);
        return isValid;
    }

    function checkLaptopDetails(laptopDetails) {
        var isValid = true;
        if (laptopDetails) {
            if (laptopDetails.screenSize) {
                laptopDetails.screenSize = laptopDetails.screenSize.toString();
                if (!_validator.isInt(laptopDetails.screenSize, {min: 10, max: 30}))
                    isValid = false;
            }
            if (laptopDetails.os) {
                laptopDetails.os = laptopDetails.os.toString();
                if (!_validator.isIn(laptopDetails.os, ['xp', 'windows vista', 'windows 7', 'windows 8', 'windows 10', 'linux']))
                    isValid = false;
            }
            if (laptopDetails.hdd) {
                laptopDetails.hdd = laptopDetails.hdd.toString();
                if (!_validator.isInt(laptopDetails.hdd, {min: 60, max: 10000}))
                    isValid = false;
            }
            if (laptopDetails.ram) {
                laptopDetails.ram = laptopDetails.ram.toString();
                if (!_validator.isInt(laptopDetails.ram, {min: 1, max: 50}))
                    isValid = false;
            }
            if (laptopDetails.cpu) {
                laptopDetails.cpu = laptopDetails.cpu.toString();
                if (!_validator.isIn(laptopDetails.cpu, ['dual core', 'core 2 duo', 'i3', 'i5', 'i7']))
                    isValid = false;
            }
        }
        return isValid;
    }

    function checkEmail(email){
        return email && _validator.isEmail(email);
    }

    return {
        checkLaptop: function (laptop, cb) {
            if (laptop && checkProduct(laptop) && checkLaptopDetails(laptop.productDetails)) {
                cb(null, new _laptop(laptop.buyPrice, laptop.sellPrice, laptop.brand, laptop.weight,
                    laptop.amount, laptop.productDetails.screenSize, laptop.productDetails.os,
                    laptop.productDetails.hdd, laptop.productDetails.ram, laptop.productDetails.cpu));
            } else {
                cb(new _customError(_objectError.inputDataError))
            }
        },
        checkLogInFields: function (email, password, cb) {
            if (email && password && checkEmail(email)) {
                cb(null, true);
            } else {
                cb(new _customError(_objectError.inputDataError))
            }
        }
    }
}());

module.exports = Validator;