/**
 * Created by hdf on 05/12/2016.
 */
var express = require('express');
var router = express.Router();
var tokenMiddleware = require("../controllers/jsonWebTokenMiddleware");
var validator = require("../controllers/validator");
var AdminService = require('../../services/admin.service');
var adminService = new AdminService();
var conf = require('../../../confenv');

router.post('/authenticate', function (req, res, next) {
    var email = req.body.email;
    var password = req.body.password;
    validator.checkLogInFields(email, password, function(err, isValide){
        if(err){
            res.json(err);
        }else{
            adminService.authenticate(email, password, function (err, admin) {
                if(err){
                    res.json(err);
                }else{
                    var token = jwt.sign(admin, conf.superSecretKey, {expiresIn: '7 days'});
                    res.json(token);
                }
            })
        }
    });
});