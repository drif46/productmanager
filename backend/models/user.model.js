/**
 * Created by hdf on 05/12/2016.
 */
var mongoose = require('../../db');

var userSchema = {
    type: {type: String},
    firstname: {type: String},
    lastname: {type: String},
    email: {type: String},
    password: {type: String}
};

module.exports = mongoose.model('User', userSchema);