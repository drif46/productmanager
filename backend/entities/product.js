/**
 * Created by drif hosseine on 12/11/2016.
 */
function Product(category, buyPrice, sellPrice, brand, weight, amount, productDetails){
    this.category = category;
    this.buyPrice = buyPrice;
    this.sellPrice = sellPrice;
    this.brand = brand;
    this.weight = weight;
    this.amount = amount;
    this.productDetails = productDetails;
}

//getters methods
Product.prototype.getCategory = function(){
    return this.category;
};

Product.prototype.getBuyPrice = function(){
    return this.buyPrice;
};

Product.prototype.getSellPrice = function(){
    return this.sellPrice;
};

Product.prototype.getBrand = function(){
    return this.brand;
};

Product.prototype.getWeight = function(){
    return this.weight;
};

Product.prototype.getProductDetails = function(){
    return this.productDetails;
};

Product.prototype.getAmount = function(){
  return this.amount;
};
//setters methods
Product.prototype.setCategory = function(category){
    this.category = category;
};

Product.prototype.setBuyPrice = function(buyPrice){
    this.buyPrice = buyPrice;
};

Product.prototype.setSellPrice = function(sellPrice){
    this.sellPrice = sellPrice;
};

Product.prototype.setBrand = function(brand){
    this.brand = brand;
};

Product.prototype.setWeight = function(weight){
    this.weight = weight;
};

Product.prototype.setProductDetails = function(productDetails){
    this.productDetails = productDetails;
};

Product.prototype.setAmount = function(amount){
    this.amount = amount;
};

Product.prototype.setCreationDate = function(creationDate){
    this.creationDate = creationDate;
}
;

module.exports = Product;