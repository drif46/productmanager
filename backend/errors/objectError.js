/**
 * Created by drif hosseine on 14/11/2016.
 */
module.exports = {
     serverDbError: {name: 'ServeurDbError', code: 1000, message: 'Database server connexion error.'},
     productCreationError: {name: 'ProductCreationError', code: 2000, message: 'An error occurred while creating a product.'},
     productFindError: {name: 'ProductFindError', code: 2001, message: 'an error occurred while fetching products.'},
     productTypeError : {name: 'ProductTypeError', code: 2002, message: 'The entity must be a product.'},
     laptopTypeError : {name: 'LaptopTypeError', code: 2102, message: 'The entity must be a laptop.'},
     createRightError : {name: 'CreateRightError', code: 3000, message: 'The User can not create the entity.'},
     userAuthenticateError : {name: 'UserAuthenticateError', code: 4000, message: 'Failed to authenticate token.'},
     userCreationError: {name: 'UserCreationError', code: 4000, message: 'An error occurred while creating an user.'},
     userTypeError : {name: 'UserTypeError', code: 4002, message: 'The entity must be an user.'},
     userFindError: {name: 'UserFindError', code: 2001, message: 'an error occurred while fetching user.'},
     adminTypeError : {name: 'AdminTypeError', code: 4102, message: 'The entity must be an admin.'},
     adminAuthenticateError : {name: 'AdminTypeError', code: 4103, message: 'Failed to authenticate the admin.'},
     tokenError : {name: 'TokenError', code: 4001, message: 'No token provided.'},
     inputDataError : {name: 'InputDataError', code: 5000, message: 'the data sent are incorrect.'}
};
