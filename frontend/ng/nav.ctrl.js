/**
 * Created by drif hosseine on 04/12/2016.
 */
angular.module('app').controller('NavCtrl', function($scope, $uibModal){

    $scope.openLogInModal = function (user) {
        //ouverture  + initialisation
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            size: 'sm',
            //partial et controller dédié
            templateUrl: 'logIn.html',
            controller: 'LogInCtrl'
        });
    }
});
