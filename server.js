/**
 * Created by drif hosseine on 24/09/2016.
 */
var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();
var app = express();
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});

app.use(express.static(__dirname + '/assets'));
app.use(express.static(__dirname + '/frontend/partials'));
app.use('/fonts', express.static(__dirname + '/frontend/fonts'));

app.listen(8099, function () {
    console.log('Server listening on', 8099);
});


