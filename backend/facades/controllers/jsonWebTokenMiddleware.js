/**
 * Created by drif hosseine on 22/11/2016.
 */
var jwt = require("jsonwebtoken");
var customError = require('../errors/customError');
var objectError = require('../errors/objectError');
var Config = require('../confenv'),
    conf = new Config();
var admin = require("../../entities/admin");
var seller = require("../../entities/seller");

module.exports = {
    checkToken: function (req, res, next) {
        // check header or url parameters or post parameters for token
        var token = req.headers['x-auth-token'] || req.headers['X-AUTH-TOKEN'];
        if (token) {
            jwt.verify(token, conf.superSecretKey, function (err, decoded) {
                if (err)
                    return res.json(new customError(objectError.userAuthenticateError));
                else{
                    var actor;
                    switch (decoded.type) {
                        case 'Admin':
                            actor = new admin(decode.firstname, decode.lastname, decode.email);
                            break;
                        case 'Seller':
                            actor = new seller(decode.firstname, decode.lastname, decode.email);
                            break;
                        default:
                            return res.json(new customError(objectError.userAuthenticateError));
                            break;
                    }
                    req.body.actor = actor;
                    next();
                }
            });
        } else {
            return res.json(new customError(objectError.tokenError));
        }
    }
};