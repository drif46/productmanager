/**
 * Created by drif hosseine on 14/11/2016.
 */
function CustomError(objectError){
    this.errorCode = objectError.code;
    this.name = objectError.name;
    this.message = objectError.message;
    Error.captureStackTrace(this, CustomError);
}

CustomError.prototype = Object.create(Error.prototype);
CustomError.prototype.constructor = CustomError;

CustomError.prototype.getErrorCode = function(){
    return this.errorCode;
};

CustomError.prototype.setErrorCode = function(errorCode){
    this.errorCode = errorCode;
};
module.exports = CustomError;