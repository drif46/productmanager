/**
 * Created by drif hosseine on 22/11/2016.
 */
var express = require('express');
var router = express.Router();
var tokenMiddleware = require("../controllers/jsonWebTokenMiddleware");
var validator = require("../controllers/validator");
var LaptopService = require('../../services/laptop.service');
var laptopService = new LaptopService();

router.post('/add', function (req, res, next) {
    tokenMiddleware.checkToken(req, res, next);
}, function (req, res, next) {
    var actor = req.body.actor;
    validator.checkLaptop(req.body, function(err, laptop){
        if(err){
            res.json(err);
        }else{
            laptopService.createLaptop(actor, laptop, function (err, result) {
                if(err){
                    res.json(err);
                }else{
                    res.json(result);
                }
            })
        }
    });
});

router.get('/all', function(req, res, next){
    tokenMiddleware.checkToken(req, res, next);
    var actor = req.body.actor;
    var laptopService = new LaptopService();
    laptopService.listAllLaptop(actor, function(err, result){
        if(err){
            res.json(err);
        }else{
            res.json(result);
        }
    })
});