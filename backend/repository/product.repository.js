/**
 * Created by drif hosseine on 12/11/2016.
 */
var productType = require('../entities/product');
var customError = require('../errors/customError');
var objectError = require('../errors/objectError');

function ProductRepository() {
    this.productModel = require('../models/product.model');
}

ProductRepository.prototype.createProduct = function (product, cb) {
    if (product instanceof productType) {
        this.productModel.create(product, function (err, result) {
            if (err) {
                return cb(new customError(objectError.productCreationError));
            }
            else {
                cb(null, result);
            }
        });
    } else {
        return cb(new customError(objectError.productTypeError));
    }
};

ProductRepository.prototype.listAllProducts = function (cb) {
    this.productModel.find(function (err, result) {
        if (err) {
            return cb(new customError(objectError.productFindError));
        } else {
            cb(null, result);
        }
    });
};

ProductRepository.prototype.findByCategory = function (category, cb) {
    this.productModel.find({category: category}, function (err, result) {
        if (err) {
            return cb(new customError(objectError.productFindError));
        } else {
            cb(null, result);
        }
    });
};

module.exports = ProductRepository;