/**
 * Created by hdf on 05/12/2016.
 */
var Admin = require("../entities/admin");
function AdminBusinessService(){
    var AdminRepository = require("../repository/admin.repository");
    this.adminRepository = new AdminRepository();
}

AdminBusinessService.prototype.createAdmin = function(admin, cb){
    this.adminRepository.createAdmmin(admin, cb);
};

AdminBusinessService.prototype.authenticate = function(email, passwor, cb){
    this.adminRepository.authenticate(email, passwor, function(err, result){
        if(err){
            return cb(err);
        }else{
            var admin = new Admin(result.firstname, result.lastname, result.email);
            return cb(null, admin);
        }
    });
};

module.exports = AdminBusinessService;