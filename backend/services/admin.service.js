/**
 * Created by hdf on 05/12/2016.
 */
var customError = require('../errors/customError');
var objectError = require('../errors/objectError');
function AdminService (){
    var adminBusinessService = require("../business_services/admin.businessService");
    this.adminBusinessService = new adminBusinessService();
}

AdminService.prototype.createAdmin = function(actor, admin, cb){
    if(rac.isAutorized(actor, admin, 'create')){
        this.adminBusinessService.createAdmin(admin, cb)
    }else{
        return cb(new customError(objectError.createRightError));
    }
};

AdminService.prototype.authenticate = function(email, passwor, cb){
    this.adminBusinessService.authenticate(email, passwor, cb);
};

module.exports = AdminService;