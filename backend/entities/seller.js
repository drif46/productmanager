/**
 * Created by drif hosseine on 21/11/2016.
 */
var User = require('./user');

function Seller(firstname, lastname, email, password){
    User.call(this, 'Seller', firstname, lastname, email, password);
}

Seller.prototype = Object.create(User.prototype);
Seller.prototype.constructor = Seller;

module.exports = Seller;
