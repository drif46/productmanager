/**
 * Created by hdf on 05/12/2016.
 */
var userType = require('../entities/user');
var customError = require('../errors/customError');
var objectError = require('../errors/objectError');

function UserRepository() {
    this.userModel = require('../models/user.model');
}

UserRepository.prototype.createUser = function (user, cb) {
    if (user instanceof userType) {
        this.userModel.create(user, function (err, result) {
            if (err) {
                return cb(new customError(objectError.userCreationError));
            }
            else {
                cb(null, result);
            }
        });
    } else {
        return cb(new customError(objectError.userTypeError));
    }
};

UserRepository.prototype.findByEmailAndPassword = function (email, password, cb) {
    this.userModel.find({email: email, password: password}, function (err, result) {
        if (err) {
            return cb(new customError(objectError.userFindError));
        } else {
            cb(null, result);
        }
    });
};

module.exports = UserRepository;
