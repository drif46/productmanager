/**
 * Created by drif hosseine on 20/11/2016.
 */
var customError = require('../errors/customError');
var objectError = require('../errors/objectError');
var rac = require("./rac");

function LaptopService (){
    var laptopBusinessService = require("../business_services/laptop.businessService");
    this.laptopBusinessService = new laptopBusinessService();
}

LaptopService.prototype.createLaptop = function(actor, laptop, cb){
    if(rac.isAutorized(actor, laptop, 'create')){
        this.laptopBusinessService.createLaptop(laptop, cb);
    }else{
        return cb(new customError(objectError.createRightError));
    }
};

LaptopService.prototype.listAllLaptop = function(actor, cb){
    if(rac.isAutorized(actor, 'laptop', 'find')){
        this.laptopBusinessService.listAllLaptop(cb);
    }else{
        return cb(new customError(objectError.createRightError));
    }
};

module.exports = LaptopService;