/**
 * Created by drif hosseine on 27/09/2016.
 */
var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var ngAnnotate = require('gulp-ng-annotate');
var livereload = require('gulp-livereload');
var minifyCSS = require('gulp-minify-css');
var nodemon = require('gulp-nodemon');
var flatten = require('gulp-flatten');
var uglify = require('gulp-uglify')

gulp.task('js', function () {
    gulp.src(['frontend/ng/module.js', 'frontend/ng/**/*.js'])
        .pipe(sourcemaps.init())
        .pipe(concat('app.min.js'))
        // .pipe(ngAnnotate())
        // .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('assets'))
        .pipe(livereload());
});

gulp.task('css', function () {
    gulp.src('frontend/css/**/*.css')
        .pipe(concat('app.min.css'))
        .pipe(minifyCSS({keepBreaks: true}))
        .pipe(gulp.dest('assets'))
        .pipe(livereload());
});

gulp.task('bower', function () {
    gulp.src('bower_components/**/*.min.js')
        .pipe(flatten())
        .pipe(gulp.dest('assets'))
        .pipe(livereload());

    gulp.src('bower_components/**/*.map')
        .pipe(flatten())
        .pipe(gulp.dest('assets'))
        .pipe(livereload());

    // gulp.src(['bower_components/**/*.js', '!bower_components/**/*.min.js'])
    //     .pipe(flatten())
    //     .pipe(gulp.dest('assets'))
    //     .pipe(livereload());

    gulp.src(['bower_components/**/*.css','bower_components/**/*.html'])
        .pipe(flatten())
        .pipe(gulp.dest('assets'))
        .pipe(livereload());

    gulp.src('frontend/img/**/*')
        .pipe(flatten())
        .pipe(gulp.dest('assets'))
        .pipe(livereload());
});

gulp.task('watch:js', function () {
    gulp.watch('frontend/ng/**/*.js', ['js']);
});

gulp.task('watch:css', function () {
    gulp.watch('frontend/css/**/*.css', ['css']);
});

gulp.task('build', ['js', 'css', 'bower']);

gulp.task('dev', ['build', 'watch:css', 'watch:js', 'bower', 'dev:server']);

gulp.task('dev:server', function () {
    livereload.listen();
    nodemon({
        script: 'server.js',
        ext: 'js',
        ignore: ['frontend*', 'assets*']
    });
});

gulp.task('default', function() {
    // place code for your default task here
});