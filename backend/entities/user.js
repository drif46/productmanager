/**
 * Created by drif hosseine on 21/11/2016.
 */
function User(type, firstname, lastname, email, password){
    this.type = type;
    this.firstname = firstname;
    this.lastname = lastname;
    this.email = email;
    this.password = password;
}

//getters methods
User.prototype.getType = function(){
    return this.type;
};

User.prototype.getFirstname = function(){
    return this.firstname;
};

User.prototype.getLastname = function(){
    return this.lastname;
};

User.prototype.getEmail = function(){
    return this.email;
};

User.prototype.getPassword = function(){
    return this.password;
};

//setters methods
User.prototype.setType = function(type){
    this.type = type;
};

User.prototype.setFirstnmae = function(firstname){
    this.firstname = firstname;
};

User.prototype.setLastname = function(lastname){
    this.lastname = lastname;
};

User.prototype.setEmail = function(email){
    this.email = email;
};

User.prototype.setPassword = function(password){
    this.password = password;
};

module.exports = User;