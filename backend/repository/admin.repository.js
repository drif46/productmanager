/**
 * Created by hdf on 05/12/2016.
 */
var adminType = require('../entities/admin');
var customError = require('../errors/customError');
var objectError = require('../errors/objectError');

function AdminRepository() {
    var userRepository = require('../repository/user.repository');
    this.userRepository = new userRepository();
}

AdminRepository.prototype.createAdmin = function (admin, cb) {
    if (admin instanceof adminType) {
        this.userRepository.createUser(admin, cb);
    } else {
        cb(new customError(objectError.adminTypeError));
    }
};

AdminRepository.prototype.authenticate = function (email, passwor, cb) {
    this.userRepository.findByEmailAndPassword(email, passwor, function(err, result){
        if(err){
            return cb(err);
        }else{
            if(!result || result.length == 0){
                return cb(new customError(objectError.adminAuthenticateError));
            }else{
                cb(null, result[0]);
            }
        }
    });
};

module.exports = AdminRepository;
