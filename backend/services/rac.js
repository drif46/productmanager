/**
 * Created by drif hosseine on 20/11/2016.
 */
var Rac= (function(){
    var _laptop = require("../entities/laptop");
    var _admin = require("../entities/admin");
    var _seller = require("../entities/seller");
    var _getResourceType = function(resource){
        var type;
        if(resource instanceof _laptop){
            type = 'laptop';
        }
        return type;
    };

    var _getActorType = function(actor){
        var type;
        if(actor instanceof _admin){
            type = 'admin';
        }
        else if(actor instanceof  _seller){
            type = 'seller';
        }
        return type;
    };

    var _accessRules = {
        admin:{
            create : {laptop: true, admin: true},
            delete : {laptop: true},
            find : {laptop: true},
            update : {laptop: true}
        },
        seller: {
            find : {laptop: true}
        }
    };
    return{
        isAutorized: function(actor, resource, action){
            var actorRules = _accessRules[_getActorType(actor)];
            var actionRules = actorRules && actorRules[action];
            var isAutorised = actionRules && actionRules[_getResourceType(resource)];
            return isAutorised;
        }
    }
})();

module.exports = Rac;