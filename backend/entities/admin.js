/**
 * Created by drif hosseine on 21/11/2016.
 */
var User = require('./user');

function Admin(firstname, lastname, email, password){
    User.call(this, 'Admin', firstname, lastname, email, password);
}

Admin.prototype = Object.create(User.prototype);
Admin.prototype.constructor = Admin;

module.exports = Admin;