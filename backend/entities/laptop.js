/**
 * Created by drif hosseine on 12/11/2016.
 */
var Product = require('./product.js');

function Laptop(buyPrice, sellPrice, brand, weight, amount, screenSize, os, hdd, ram, cpu){
    var productDetails = {
        screenSize : screenSize,
        os : os,
        hdd : hdd,
        ram : ram,
        cpu : cpu
    };
    Product.call(this, 'Laptop', buyPrice, sellPrice, brand, weight, amount, productDetails);
}

Laptop.prototype = Object.create(Product.prototype);
Laptop.prototype.constructor = Laptop;
//gettres methods
Laptop.prototype.getScreenSize = function(){
    return this.productDetails.screenSize;
};

Laptop.prototype.getOs = function(){
    return this.productDetails.os;
};

Laptop.prototype.getHdd= function(){
    return this.productDetails.hdd;
};

Laptop.prototype.getRam = function(){
    return this.productDetails.ram;
};

Laptop.prototype.getCpu = function(){
    return this.productDetails.cpu;
};
//setters methods
Laptop.prototype.setScreenSize = function(screenSize){
    this.productDetails.screenSize = screenSize;
};

Laptop.prototype.setOs = function(os){
    this.productDetails.os = os;
};

Laptop.prototype.setHdd= function(hdd){
    this.productDetails.hdd = hdd;
};

Laptop.prototype.setRam = function(ram){
    this.productDetails.ram = ram;
};

Laptop.prototype.setCpu = function(cpu){
    this.productDetails.cpu = cpu;
};

module.exports = Laptop;