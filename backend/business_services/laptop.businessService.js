/**
 * Created by drif hosseine on 20/11/2016.
 */
function LaptopBusinessService(){
    var laptopRepository = require("../repository/laptop.repository");
    this.laptopRepository = new laptopRepository();
}

LaptopBusinessService.prototype.createLaptop = function(laptop, cb){
    laptop.setCreationDate(new Date());
    this.laptopRepository.createLaptop(laptop, cb);
};

LaptopBusinessService.prototype.listAllLaptop = function(cb){
    this.laptopRepository.listAllLaptop(cb);
};

module.exports = LaptopBusinessService;